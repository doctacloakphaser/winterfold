var w = 2560,
    h = 1195;

/*
For Fullscreen put this code:

var w = window.innerWidth * window.devicePixelRatio,
    h = window.innerHeight * window.devicePixelRatio;
*/





var game = new Phaser.Game(w, h, Phaser.AUTO, 'gameContainer');

//global object for font loading
game.WebFont = {
  //call rungame when fonts are loaded
  active: function() {
		runGame()
  },
  custom: {
    //array of family names, the ones written within the stylesheet.css coming
    //in the fontSquirrel's webfont kit
    families: ['sumiremedium'],
    //local path to stylesheet.css
    urls: ["css/fonts/stylesheet.css"]
  }
};

game.state.add('BootState', BootState);
game.state.add('PreloaderState', PreloaderState);
game.state.add('MovieState', MovieState);
game.state.add('MainMenuState', MainMenuState);
game.state.add('TestState', TestState);

game.state.start('BootState');

var BootState = {
    init: function() {
        // Sets aspect ratio that can scale accordingly.
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Center the game canvas horizintally and vertically.
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        this.scale.setMaximum();
    },
    preload: function() {
        // Load Preload Bar
        this.load.atlasJSONHash('preloadBar', 'WebContent/assets/img/StanixIntro.png', 'WebContent/assets/img/StanixIntro.json');
        this.load.atlasJSONHash('MMBackground', 'WebContent/assets/img/MMBackground.png', 'WebContent/assets/img/MMBackground.json');
    },
    create: function() {
        //Initial GameSystem (Arcade, P2, Ninja)
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // Change BG color to White
        this.game.stage.backgroundColor = '#000';
        // Start Preload State
        this.state.start('PreloaderState');

        //HACK TO PRELOAD SUMIRE CUSTOM FONT
        this.game.add.text(0, 0, "hack", {font:"30px sumiremedium", fill:"#FFFFFF"});
    }



};

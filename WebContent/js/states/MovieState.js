
var MovieState = {
    preload: function() {
        this.game.stage.backgroundColor = '#000';
        this.video = game.add.video('winterfold-intro');

        console.log(this.game.height);
        //  See the docs for the full parameters
        //  But it goes x, y, anchor x, anchor y, scale x, scale y
        console.log(this.video);
        console.log(this);
        this.sprite = this.video.addToWorld(this.game.world.centerX, this.game.world.centerY, 0.5, 0.5);
        console.log(this.sprite.width);

        //  false = loop
        this.video.play(false);

        game.input.onDown.add(this.nextState, this);

    },
    nextState: function() {
        this.video.destroy();
        this.state.start('MainMenuState');
    }
};

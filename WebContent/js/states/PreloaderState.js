var PreloaderState = {

    preload: function () {

        /*
        Load all game assets
        Place your load bar, some messages.
        In this case of loading, only text is placed...
        */

        // Preload Bar
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadBar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.animations.add('loading', [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0]);
        this.preloadBar.animations.play('loading', 20, false);

        // var loadingLabel = game.add.text(80, 150, 'loading...', {font: '30px Courier', fill: '#fff'});

        //Load your images, spritesheets, bitmaps...
        this.load.image('winterfold-logo', 'WebContent/assets/img/winterfold-logo.png')
        this.load.image('bgtest', 'WebContent/assets/img/bgtest.png');

        //Load your sounds, efx, music...
        //Example: game.load.audio('rockas', 'assets/snd/rockas.wav');
        this.load.audio('fireside', ['WebContent/assets/audio/fireside.mp3', 'WebContent/assets/audio/fireside.ogg']);
        this.load.audio('mmhover', ['WebContent/assets/audio/mmhover.ogg']);
        this.load.video('winterfold-intro', 'WebContent/assets/video/winterfold.webm');

        //Load your data, JSON, Querys...
        //Example: game.load.json('version', 'http://phaser.io/version.json');

    },
    create: function () {
        this.game.stage.backgroundColor = '#000';

         //A simple fade out effect
         this.game.time.events.add(Phaser.Timer.SECOND * 0.8, function() {
             var tween = this.add.tween(this.preloadBar)
                 .to({alpha: 0}, 750, Phaser.Easing.Linear.none);

           tween.onComplete.add(function() {
                 this.preloadBar.destroy();
                 this.startGame();
             }, this);

           tween.start();
         }, this);
    },
    startGame: function() {
       this.state.start('MovieState');
   },

};
